package com.clinica.api;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.clinica.model.Persona;
import com.clinica.service.IPersonaService;

////JAX RS 2.1
@Path("/personas")
public class PublicacionAPI {
	
	@Inject
	private IPersonaService service;	
	private Persona personas;
	private List<Persona> personasall;
	
	//localhost:8080/RESTClinica/rest/personas/listar/2
	@GET
	@Path("/listar/{id}")
	@Produces("application/json")
	public Response listar(@PathParam("id") Integer id) {
		try {
			Persona per = new Persona();
			per.setIdPersona(id);
			this.personas = this.service.listarPorId(per);			
		}catch(Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.status(Response.Status.OK).entity(personas).build();
	}
	
	@POST
	@Path("/registrar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response registrar(Persona p) {
		try {
			int rpta = this.service.registrar(p);
			
		} catch (Exception e) {			
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} 
		return Response.status(Response.Status.OK).entity(p).build();
	}
	
	@POST
	@Path("/modificar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response modificar(Persona p) {
		try {
			int rpta = this.service.modificar(p);
			
		} catch (Exception e) {			
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} 
		return Response.status(Response.Status.OK).entity(p).build();
	}
	
	
	@GET
	@Path("/listar")
	@Produces("application/json")
	public Response listartodo() {
		try {
			this.personasall = this.service.listar();			
		}catch(Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.status(Response.Status.OK).entity(personasall).build();
	}

}
